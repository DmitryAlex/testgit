/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "delay_t.h"

/* Private defines -----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void main(void)
{
  // ������� ���������� 16���
  CLK_DeInit();
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
  CLK_HSICmd(ENABLE);
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
  CLK_ClockSwitchCmd(ENABLE);

  // ������������� �������� ��� ���������� "delay_t.h"
  CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER2, ENABLE);
  TIM2_TimeBaseInit(TIM2_PRESCALER_1, 16);
  TIM2_Cmd(ENABLE);

  CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER4, ENABLE);
  TIM4_TimeBaseInit(TIM4_PRESCALER_64, 250);
  TIM4_Cmd(ENABLE);
  TIM4_Cmd(ENABLE);

  /* Infinite loop */
  while (1)
  {
	  // ������ ������
	  // ������ ������
	  // ������� ����� slave
	  // ������� ������ 1 � slave
	  
	    // ������������� �������� ��� ���������� "delay_t.h"
  CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER2, ENABLE);
  TIM2_TimeBaseInit(TIM2_PRESCALER_1, 16);
  TIM2_Cmd(ENABLE);

	  // ����� ������
	  int branch = 0;
	  branch = 5;
  }

}

void Timer(void)
{
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER4, ENABLE);
  TIM4_TimeBaseInit(TIM4_PRESCALER_64, 250);
  TIM4_Cmd(ENABLE);
  TIM4_Cmd(ENABLE);
  // 12345
  CLK_DeInit();
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
  // master
}

void Fun(void)
{
	int a;
	a = 10;
	a = 11;
	int b;
	b = a + 5;
	// master
}

void Uart(void)
{
	CLK_DeInit();
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
  CLK_HSICmd(ENABLE);
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
  CLK_ClockSwitchCmd(ENABLE);
  
  // Send
  SendData(5);
  
  // Receive
  int data;
  data = Receive();
  
  // Add
  int c = 0;
  
  // Add fault
  int fault = 0;
  fault = c + data;
  fault = 5;
}

void Foo(void)
{
	/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
  uint16_t qwerty = 0;
  qwerty = 50;
  
  qwerty = 150 + 123;
  
  int but = 0;
  but = qwerty;
  but++;
  but = 5;
  but = 6;
}

void test(void)
{
	int mas[] = {12345,94797,93784};
	
	for(i=0; i<100; i++)
	{
		mas[i] = 0;
	}
	
	mas[i] = 56;
	
	while(1)
	{
		for(i=0; i<100; i++)
		{
			mas[i] = 0;
		}
	}
}

void help(int h)
{
	int a = h;
	
	h = 2 + 6;
	h += mas;
	
	for(i=0; i<100; i++)
	{
		h = i + 1;
	}
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
